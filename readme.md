# Smiling-face-recognition

Ce projet a pour but d'entrainer un modèle de deep learning à reconnaitre des visages souriants de visage non souriants.

Les images sont d'abord préparées avec pytorch pour supprimer le fond puis avec openCV pour détécter le/les visages présents sur l'image. On ne garde que la première image.

Pour entrainer le modèle, on utilise les photos du dataset https://paperswithcode.com/dataset/celeba-hq.

On utilise ensuite openCV pour tester notre modèle avec des images de la webcam, qui détecte en live si le visage est souriant ou non.


## Tester ce rprojet avec binder

L'installation d'openCV pour ếtre hébérgée a demandé quelques recherches. La configuration se fait grâce au fichier environment.yml qui contient les dépendances python et au fichier apt.txt qui indique les paquets à installer sur l'image docker.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/jtobelem1%2Fsmiling-face-recognition/HEAD?urlpath=lab)